module ApplicationHelper

  def breadcrumb_seperator
    return " ＞ "
  end

  def link_bar(direction, string)
    str = '<ul class="table-view">'
    str += '<li class="table-view-cell media">'
    str += "<a class='navigate-left' href='#{direction}' data-transition='slide-in'>"
    str += '<div class="media-body">' + string + '</div></a></li></ul>'
    return str.html_safe
  end

  def back_bar
    str = '<ul class="table-view">'
    str += '<li class="table-view-cell media">'
    str += '<a class="navigate-left" data-transition="slide-in" onClick="history.back();">'
    str += '<div class="media-body">' + I18n.t('operate.back') + '</div></a></li></ul>'
    return str.html_safe
  end

  def form_cancel_button
    str = '<div class="content-padded">'
    str +=   '<a class="navigate-left" data-transition="slide-in" onClick="history.back();">'
    str +=     '<div class="btn btn-block">'
    str +=       'キャンセル'
    str +=     '</div>'
    str +=   '</a>'
    str += '</div>'
    return str.html_safe
  end

  def date_ymd(time)
    return time unless time
    time.strftime("%Y/%m/%d")
  end

  def datetime_ymdhm(datetime)
    return datetime unless datetime
    datetime.strftime("%Y/%m/%d  %H:%M")
  end

end
