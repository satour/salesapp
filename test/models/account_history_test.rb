# == Schema Information
#
# Table name: account_histories
#
#  id             :integer          not null, primary key
#  account_id     :integer
#  name           :string(255)
#  tel_number     :string(255)
#  fax_number     :string(255)
#  address        :string(255)
#  website        :string(255)
#  representative :string(255)
#  capital        :integer
#  created_at     :datetime
#  updated_at     :datetime
#

require "test_helper"

class AccountHistoryTest < ActiveSupport::TestCase

  def account_history
    @account_history ||= AccountHistory.new
  end

  def test_valid
    assert account_history.valid?
  end

end
