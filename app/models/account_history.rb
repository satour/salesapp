# == Schema Information
#
# Table name: account_histories
#
#  id             :integer          not null, primary key
#  account_id     :integer
#  name           :string(255)
#  tel_number     :string(255)
#  fax_number     :string(255)
#  address        :string(255)
#  website        :string(255)
#  representative :string(255)
#  capital        :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class AccountHistory < ActiveRecord::Base
  belongs_to :account

  #scope---------------------------------------------------------
  scope :of, ->(account_id) {
    where(account_id: account_id).order("created_at DESC")
  }

end
