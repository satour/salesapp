json.extract! @contact, :id, :last_name, :first_name, :email, :tel_number, :fax_number, :address, :created_at, :updated_at
