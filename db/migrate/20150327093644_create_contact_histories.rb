class CreateContactHistories < ActiveRecord::Migration
  def change
    create_table :contact_histories do |t|
      t.references :contact, index: true
      t.string :last_name
      t.string :first_name
      t.string :email
      t.string :tel_number
      t.string :fax_number
      t.string :address

      t.timestamps
    end
  end
end
