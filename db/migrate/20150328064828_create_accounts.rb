class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :tel_number
      t.string :fax_number
      t.string :address
      t.string :website
      t.string :representative
      t.integer :capital

      t.timestamps
    end
  end
end
