# == Schema Information
#
# Table name: contact_histories
#
#  id         :integer          not null, primary key
#  contact_id :integer
#  last_name  :string(255)
#  first_name :string(255)
#  email      :string(255)
#  tel_number :string(255)
#  fax_number :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class ContactHistory < ActiveRecord::Base
  belongs_to :contact

  #scope---------------------------------------------------------
  scope :of, ->(contact_id) {
    where(contact_id: contact_id).order("created_at DESC")
  }

end
