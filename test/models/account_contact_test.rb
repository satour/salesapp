# == Schema Information
#
# Table name: account_contacts
#
#  id         :integer          not null, primary key
#  account_id :integer
#  contact_id :integer
#  created_at :datetime
#  updated_at :datetime
#

require "test_helper"

class AccountContactTest < ActiveSupport::TestCase

  def account_contact
    @account_contact ||= AccountContact.new
  end

  def test_valid
    assert account_contact.valid?
  end

end
