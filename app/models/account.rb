# == Schema Information
#
# Table name: accounts
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  tel_number     :string(255)
#  fax_number     :string(255)
#  address        :string(255)
#  website        :string(255)
#  representative :string(255)
#  capital        :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Account < ActiveRecord::Base
  #association---------------------------------------------------------
  has_many :account_histories
  has_many :account_contacts
  has_many :contacts, :through => :account_contacts

  #validation ---------------------------------------------------------
  #TODO:impl validation

  #scope---------------------------------------------------------
  scope :which_this_contact_belongs_to, ->(contact_id) {
    select(:id, :name)
    .includes(:account_contacts)
    .where(account_contacts: {contact_id: contact_id })
  }

end
