json.array!(@accounts) do |account|
  json.extract! account, :id, :name, :tel_number, :fax_number, :address, :website, :representative, :capital
  json.url account_url(account, format: :json)
end
