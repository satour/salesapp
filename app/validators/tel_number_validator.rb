class TelNumberValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.present?
      unless value =~ /\+?\d{0,2}\d{2,4}-\d{2,4}-\d{2,4}/  #TODO:海外番号対応
        record.errors[attribute] << (options[:message] || I18n.t("errors.messages.invalid_format"))
      end
    end
  end
end
