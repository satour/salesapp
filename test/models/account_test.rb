# == Schema Information
#
# Table name: accounts
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  tel_number     :string(255)
#  fax_number     :string(255)
#  address        :string(255)
#  website        :string(255)
#  representative :string(255)
#  capital        :integer
#  created_at     :datetime
#  updated_at     :datetime
#

require "test_helper"

class AccountTest < ActiveSupport::TestCase

  def account
    @account ||= Account.new
  end

  def test_valid
    assert account.valid?
  end

end
