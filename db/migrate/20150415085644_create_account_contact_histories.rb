class CreateAccountContactHistories < ActiveRecord::Migration
  def change
    create_table :account_contact_histories do |t|
      t.references :account_contact, index: true
      t.references :account, index: true
      t.references :contact, index: true

      t.timestamps
    end
  end
end
