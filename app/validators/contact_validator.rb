class ContactValidator < ActiveModel::Validator
  def validate(record)
    if record.last_name.blank? && record.first_name.blank?
      record.errors.add :base, I18n.t('models.contact.error.names_are_blank')
    end
    # record.errors.add :base, "This is some custom error message"
    # record.errors.add :first_name, "This is some complex validation"
    # etc...
  end
end
