# == Schema Information
#
# Table name: contact_histories
#
#  id         :integer          not null, primary key
#  contact_id :integer
#  last_name  :string(255)
#  first_name :string(255)
#  email      :string(255)
#  tel_number :string(255)
#  fax_number :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require "test_helper"

class ContactHistoryTest < ActiveSupport::TestCase

  def contact_history
    @contact_history ||= ContactHistory.new
  end

  def test_valid
    assert contact_history.valid?
  end

end
