class CreateAccountContacts < ActiveRecord::Migration
  def change
    create_table :account_contacts do |t|
      t.references :account, index: true
      t.references :contact, index: true

      t.timestamps
    end
  end
end
