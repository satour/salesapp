class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]
  before_action :set_related_account, only: [:show, :edit]

  # GET /contacts
  # GET /contacts.json
  def index
    if params[:keyword] && params[:keyword].present?
      cond = "%#{params[:keyword]}%"
      @all_contacts = Contact.where("last_name LIKE ? OR first_name LIKE ?", cond, cond)
      @contacts = @all_contacts.page(params[:page])
    else
      @all_contacts = Contact.all
      @contacts = @all_contacts.page(params[:page])
    end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/1/histories
  def history
    @contact = Contact.find(params[:id])
    @histories = ContactHistory.of(params[:id])
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create

    #TODO:associationも含めnewしたい

    @contact = Contact.new(contact_params)
    ActiveRecord::Base.transaction{
      respond_to do |format|
        if @contact.save

          @ac = @contact.account_contacts.new(
            contact_id: Contact.last.id,
            account_id: params[:account_contact][:account_id]
          )

          @ac.save

          format.html { redirect_to @contact, notice: I18n.t("models.contact.notice.create_success") }
          format.json { render :show, status: :created, location: @contact }
        else
          #TODO:rollback時の処理がない
          format.html { render :new }
          format.json { render json: @contact.errors, status: :unprocessable_entity }
        end
      end
    }

  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update

    @contact_history = ContactHistory.new(
      contact_id: @contact.id,
      last_name: @contact.last_name,
      first_name: @contact.first_name,
      email: @contact.email,
      tel_number: @contact.tel_number,
      fax_number: @contact.fax_number,
      address: @contact.address
    )

    ActiveRecord::Base.transaction do
      #TODO:rollback時の処理がない
      respond_to do |format|
        if @contact.update(contact_params) && @contact_history.save

          #TODO:変更履歴保持にはAccountContactHistoryも必要
          #TODO:複数テーブルにまたがって、いつの版のレコードか識別するためのキーが必要

          format.html { redirect_to @contact, notice: I18n.t("models.contact.notice.update_success") }
          format.json { render :show, status: :ok, location: @contact }
        else
          format.html { render :edit }
          format.json { render json: @contact.errors, status: :unprocessable_entity }
        end
      end
    end

  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: I18n.t("models.contact.notice.destroy_success") }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
      @ac = AccountContact.where(contact_id:params[:id]) #ない場合もある
    end

    def set_related_account
      @belonging = Account.which_this_contact_belongs_to(params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:last_name, :first_name, :email, :tel_number, :fax_number, :address)
    end

    def account_contact_params
      params.require(:account_contact).permit(:account_id)
    end
end
