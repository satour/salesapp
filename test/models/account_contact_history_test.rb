# == Schema Information
#
# Table name: account_contact_histories
#
#  id                 :integer          not null, primary key
#  account_contact_id :integer
#  account_id         :integer
#  contact_id         :integer
#  created_at         :datetime
#  updated_at         :datetime
#

require "test_helper"

class AccountContactHistoryTest < ActiveSupport::TestCase

  def account_contact_history
    @account_contact_history ||= AccountContactHistory.new
  end

  def test_valid
    assert account_contact_history.valid?
  end

end
