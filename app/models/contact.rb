# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  last_name  :string(255)
#  first_name :string(255)
#  email      :string(255)
#  tel_number :string(255)
#  fax_number :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Contact < ActiveRecord::Base
  #association---------------------------------------------------------
  has_many :contact_histories
  has_many :account_contacts
  has_many :accounts, :through => :account_contacts

  #validation---------------------------------------------------------
  include ActiveModel::Validations
  validates_with ContactValidator

  validates :last_name,
    length: { maximum: 50 }
  validates :first_name,
    length: { maximum: 50 }
  validates :email,
    email: true,
    exclusion: { in: %w(admin@example.com superuser@example.com) } #TODO:ベタ書きはよくない
  validates :tel_number,
    tel_number: true,
    length: { maximum: 18 }
  validates :fax_number,
    fax_number: true,
    length: { maximum: 18 }
  validates :address,
    length: { maximum: 255 }

end
