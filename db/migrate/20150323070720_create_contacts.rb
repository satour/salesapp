class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :last_name
      t.string :first_name
      t.string :email
      t.string :tel_number
      t.string :fax_number
      t.string :address

      t.timestamps
    end
  end
end
