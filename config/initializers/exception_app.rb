Rails.configuration.exceptions_app = ->(env) { ErrorsController.action(:raise_action_dispatch_exception).call(env) }
