# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  last_name  :string(255)
#  first_name :string(255)
#  email      :string(255)
#  tel_number :string(255)
#  fax_number :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require "test_helper"

class ContactTest < ActiveSupport::TestCase

  def contact
    @contact ||= Contact.new
  end

  def test_valid
    assert contact.valid?
  end

end
