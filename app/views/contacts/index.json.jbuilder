json.array!(@contacts) do |contact|
  json.extract! contact, :id, :last_name, :first_name, :email, :tel_number, :fax_number, :address
  json.url contact_url(contact, format: :json)
end
