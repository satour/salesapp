# == Schema Information
#
# Table name: account_contact_histories
#
#  id                 :integer          not null, primary key
#  account_contact_id :integer
#  account_id         :integer
#  contact_id         :integer
#  created_at         :datetime
#  updated_at         :datetime
#
# Command
#  rails g model AccountContactHistory account_contact:references account:references contact:references

class AccountContactHistory < ActiveRecord::Base
  belongs_to :account_contact
  belongs_to :account
  belongs_to :contact
end
