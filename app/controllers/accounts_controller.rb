class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :set_related_contact, only: [:show, :edit]

  # GET /accounts
  # GET /accounts.json
  def index
    if params[:keyword] && params[:keyword].present?
      @all_accounts = Account.where("name LIKE ?", "%#{params[:keyword]}%")
      @accounts = @all_accounts.page(params[:page])
    else
      @all_accounts = Account.all
      @accounts = @all_accounts.page(params[:page])
    end
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  def history
    @account = Account.find(params[:id])
    @histories = AccountHistory.of(params[:id])
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update

    @account_history = AccountHistory.new(
      account_id: @account.id,
      name: @account.name,
      tel_number: @account.tel_number,
      fax_number: @account.fax_number,
      address: @account.address,
      website: @account.website,
      capital: @account.capital
    )


    respond_to do |format|
      if @account.update(account_params)  && @account_history.save
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    def set_related_contact
      @contacts = Contact.includes(:account_contacts).where(account_contacts: { account_id: params[:id] })
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:name, :tel_number, :fax_number, :address, :website, :representative, :capital)
    end
end
