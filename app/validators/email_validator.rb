class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.present?
      unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
        record.errors[attribute] << (options[:message] || I18n.t("errors.messages.invalid_format"))
      end
    end

    unless value.length <= 255
      record.errors[attribute] << (options[:message] || I18n.t("errors.messages.invalid_format"))
    end

  end
end
