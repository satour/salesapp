# == Schema Information
#
# Table name: account_contacts
#
#  id         :integer          not null, primary key
#  account_id :integer
#  contact_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class AccountContact < ActiveRecord::Base
  belongs_to :account
  belongs_to :contact
  has_many   :account_contact_histories
end
